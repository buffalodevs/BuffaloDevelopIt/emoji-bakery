name := """emoji-bakery"""
organization := "com.buffalodevelopit"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

libraryDependencies += guice

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.buffalodevelopit.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.buffalodevelopit.binders._"
